/*
 * model.h
 *
 *  Created on: Jun 14, 2015
 *      Author: paul
 */

#ifndef EXAMPLES_CUSTOM_MODEL_H_
#define EXAMPLES_CUSTOM_MODEL_H_

#include "../../src/AtomicDevs.h"
#include "../../src/CoupledDEVS.h"

#include <algorithm>
#include <limits>
#include <iostream>

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/polymorphic.hpp>

namespace cdevs_examples {
enum Products {
	//There are 3 types of producgts in the economy, Gold, silver and bronze.
	kGold = 0,
	kSilver,
	kBronze
};
enum Modes
{
	kConsumerMode = 0,
	kProducerMode
};

class ConsumerMode: public cdevs::State<ConsumerMode>, public cdevs::EventCRTP<ConsumerMode, kConsumerMode>
{
public:
	ConsumerMode(std::string value = "buy");
	std::string string() const;
	std::string toXML() const;
	std::string getValue() const;
	void setValue(std::string value);

	double get_rate_of_consumption() const
	{
		return rate_of_consumption;
	}

	void set_rate_of_consumption(double rateOfConsumption)
	{
		rate_of_consumption = rateOfConsumption;
	}

private:
	std::string value_;
	double rate_of_consumption;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<ConsumerMode> >(this));
	}
};

class Consumer: public cdevs::Atomic<Consumer, ConsumerMode>
{
public:
	Consumer(std::string name);

	ConsumerMode const& ExtTransition(std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<cdevs::Port> > > inputs);
	ConsumerMode const& IntTransition();

	std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<cdevs::Port> > > OutputFunction();

	double TimeAdvance();

	std::weak_ptr<cdevs::Port> IN;
	std::weak_ptr<cdevs::Port> OUT;
private:
	ConsumerMode state_buy_ = ConsumerMode("buy");
	ConsumerMode state_sell_ = ConsumerMode("sell");
	ConsumerMode state_idle_ = ConsumerMode("idle");

	Consumer() : Consumer("") {};

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<Consumer, ConsumerMode>>(this), IN, OUT, state_buy_, state_sell_, state_idle_);
	}
};

class ProducerMode : public cdevs::State<ProducerMode>, public cdevs::EventCRTP<ProducerMode, kProducerMode>
{
public:
	ProducerMode(std::string value = "idle");
	std::string string() const;
	std::string toXML() const;

	std::string getValue() const;
	void setValue(std::string value);

	double get_rate_of_production() const
	{
		return rate_of_production;
	}

	void set_rate_of_production(double rateOfProduction)
	{
		rate_of_production = rateOfProduction;
	}

private:
	std::string value_;
	double rate_of_production;


	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<ProducerMode> >(this));
	}
};

class Producer : public cdevs::Atomic<Producer, ProducerMode>
{
public:
	Producer(std::string name);

	ProducerMode const& IntTransition();

	std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<cdevs::Port> > > OutputFunction();

	double TimeAdvance();

	std::weak_ptr<cdevs::Port> OUT;
private:
	ProducerMode state_idle_ = ProducerMode("idle");
	ProducerMode state_working_ = ProducerMode("working");
	int product_count_;
	int net_worth_;


	Producer() : Producer("") {};

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<Producer, ProducerMode>>(this), OUT, state_idle_, state_working_);
	}
};

class Economy: public cdevs::Coupled<Economy>
{
public:
	Economy(std::string name);

	std::shared_ptr<BaseDevs> Select(std::list<std::shared_ptr<BaseDevs>> imm_children) const;
	std::shared_ptr<Producer> getProducer() const;
	std::shared_ptr<Consumer> getConsumer() const;
	void AddResources(int index, int amount);
	void AddResourcePrice(int index, int amount);
private:
	//An economy has a multitude of Producers and consumers.
	std::vector<std::shared_ptr<Producer> > Producer_;
	std::vector<std::shared_ptr<Consumer> > Consumer_;
	std::vector<int> total_resources_;
	std::vector<int> resource_prices_;

	Economy() : Economy("") {};

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
//		ar(cereal::base_class<cdevs::Coupled<Economy>>(this), Producer_, Consumer_);
//		SetComponentSet({Producer_, Consumer_});
	}
};
/*
CEREAL_REGISTER_TYPE(cdevs_examples::ConsumerMode)
CEREAL_REGISTER_TYPE(cdevs_examples::Consumer)
CEREAL_REGISTER_TYPE(cdevs_examples::ProducerMode)
CEREAL_REGISTER_TYPE(cdevs_examples::Producer)
CEREAL_REGISTER_TYPE(cdevs_examples::Economy)
*/
//TODO: Why does this not work?
}


#endif /* EXAMPLES_CUSTOM_MODEL_H_ */
