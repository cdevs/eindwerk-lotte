/*
 * export.cpp
 *
 *  Created on: Jun 14, 2015
 *      Author: paul
 */

#include "model.h"

// Function to return an instance of a new TrafficSystem object
extern "C" {
#if defined(_MSC_VER) // Microsoft compiler
_declspec(dllexport)
#endif
cdevs_examples::Economy * construct()
{
	return new cdevs_examples::Economy("Economy");
}
}


