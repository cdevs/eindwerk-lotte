/*
 * export.cpp
 *
 *  Created on: 30-mei-2015
 *      Author: david
 */

#include "model.h"

// Function to return an instance of a new TrafficLight object
extern "C" {
#if defined(_MSC_VER) // Microsoft compiler
_declspec(dllexport)
#endif
cdevs_examples::atomic::TrafficLight* construct()
{
	return new cdevs_examples::atomic::TrafficLight("VerkeersLicht");
}
}
