/*
 * city_model.cpp
 *
 *  Created on: 12-jun.-2015
 *      Author: david
 */

#include "city_model.h"

namespace cdevs_examples {
namespace traffic {

City::City() : Coupled("City")
{
	collector_ = Collector::create();
	AddSubModel(collector_, 0);
}

}
}
