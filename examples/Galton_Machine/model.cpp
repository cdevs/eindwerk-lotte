/*
 * AtomicModelTest.cpp
 *
 *  Created on: 31-mrt.-2015
 *      Author: david
 */
#include "model.h"
#include <algorithm>
#include <limits>
#include <iostream>
#include <chrono>

using namespace cdevs;

namespace cdevs_examples {
namespace galton {

PinMode::PinMode()
	: left_(0), right_(0), ball_()
{
}

Pin::Pin(std::string name)
	: Atomic(name), rand_generator_(std::chrono::system_clock::now().time_since_epoch().count())
{
	IN = AddInPort("In");
	OUT_LEFT = AddOutPort("OutLeft");
	OUT_RIGHT = AddOutPort("OutR");
	state_ = std::unique_ptr<PinMode>(new PinMode());
}

const PinMode& Pin::ExtTransition(
        std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
                std::owner_less<std::weak_ptr<cdevs::Port> > > inputs)
{
	for (auto const_ev : inputs.at(IN)) {
		auto ev = std::dynamic_pointer_cast<const Ball>(const_ev);
		state_->ball_ = std::make_shared<Ball>(*ev);
	}
	return *state_;
}

const PinMode& Pin::IntTransition()
{
	return *state_;
}

std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
        std::owner_less<std::weak_ptr<cdevs::Port> > > Pin::OutputFunction()
{
	std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
	        std::owner_less<std::weak_ptr<cdevs::Port> > > out;
	if (state_->ball_) {
		std::uniform_int_distribution<int> distribution(0, 1);
		if (distribution(rand_generator_) == 0) {
			++state_->left_;
			out = {	{	OUT_LEFT, {state_->ball_}}};
		} else {
			++state_->right_;
			out = {	{	OUT_RIGHT, {state_->ball_}}};
		}
		state_->ball_.reset();
	}
	return out;
}

double Pin::TimeAdvance()
{
	if (state_->ball_) {
		return 0.00001;
	} else {
		return std::numeric_limits<double>::infinity();
	}
}

Bin::Bin(unsigned id)
	: Atomic("Bin_" + std::to_string(id))
{
	state_ = std::unique_ptr<BinMode>(new BinMode());
	IN = AddInPort("IN");
}

BinMode const& Bin::ExtTransition(
        std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
                std::owner_less<std::weak_ptr<cdevs::Port> > > inputs)
{
	for (auto const_ev : inputs.at(IN)) {
		++state_->count_;
	}
	return *state_;
}

double Bin::TimeAdvance()
{
	return std::numeric_limits<double>::infinity();
}

Generator::Generator(unsigned max_count)
	: Atomic("Generator"), max_count_(max_count)
{
	state_ = std::unique_ptr<GeneratorState>(new GeneratorState("gen_event1"));
	send_event1_ = AddOutPort("out_event1");
}

double Generator::TimeAdvance()
{
	if (state_->count_ < max_count_) {
		return 1.0;
	} else {
		return 1;
		return std::numeric_limits<double>::infinity(); //This does not work :(
	}
	return 1.0;
}

GeneratorState const& Generator::IntTransition()
{
	++state_->count_;
	return *state_;
}

std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
        std::owner_less<std::weak_ptr<cdevs::Port> > > Generator::OutputFunction()
{
	return { {	send_event1_, {std::make_shared<Ball>(state_->count_)}}};
}

GaltonMachine::GaltonMachine(unsigned nr_trays, unsigned max_count)
	: Coupled("GaltonMachine")
{
	for (unsigned i = 1; i < nr_trays; i++) {
		std::vector<std::shared_ptr<Pin>> pins;
		for (unsigned j = 0; j < i; j++) {
			std::shared_ptr<Pin> p = Pin::create("Pin_" + std::to_string(i - 1) + "_" + std::to_string(j));
			AddSubModel(p);
			pins.push_back(p);
		}
		pins_.push_back(pins);
	}
	for (unsigned i = 0; i < nr_trays; i++) {
		std::shared_ptr<Bin> b = Bin::create(i);
		AddSubModel(b);
		bins_.push_back(b);
	}
	for (unsigned k = 0; k < nr_trays - 2; k++) {
		std::vector<std::shared_ptr<Pin>> pins = pins_.at(k);
		std::vector<std::shared_ptr<Pin>> pins2 = pins_.at(k + 1);
		for (unsigned l = 0; l < pins.size(); l++) {
			ConnectPorts(pins.at(l)->OUT_LEFT, pins2.at(l)->IN);
			ConnectPorts(pins.at(l)->OUT_RIGHT, pins2.at(l + 1)->IN);
		}
	}
	std::vector<std::shared_ptr<Pin>> pins = pins_.back();
	for (unsigned l = 0; l < pins.size(); l++) {
		ConnectPorts(pins.at(l)->OUT_LEFT, bins_.at(l)->IN);
		ConnectPorts(pins.at(l)->OUT_RIGHT, bins_.at(l + 1)->IN);
	}
	generator_ = Generator::create(max_count);
	AddSubModel(generator_);
	ConnectPorts(generator_->send_event1_, pins_.front().front()->IN);
}

std::shared_ptr<BaseDevs> GaltonMachine::Select(std::list<std::shared_ptr<BaseDevs> > imm_children) const
{
	return 0;
}

}
}
