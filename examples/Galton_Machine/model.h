/*
 * model.h
 *
 *  Created on: 7-mei-2015
 *      Author: david
 */

#ifndef EXAMPLES_GALTON_MACHINE_H_
#define EXAMPLES_GALTON_MACHINE_H_

#include <random>

#include "../../src/AtomicDevs.h"
#include "../../src/CoupledDEVS.h"

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/common.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/tuple.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/details/polymorphic_impl.hpp>

namespace cdevs_examples {
namespace galton {

class Ball: public cdevs::EventCRTP<Ball, 0>
{
	friend class cereal::access;
public:
	Ball(unsigned nr = 0)
		: nr_(nr)
	{
	}
	Ball(const Ball& other)
		: nr_(other.nr_)
	{
	}
	std::string string() const
	{
		return "";
	}
private:
	unsigned nr_;
	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::EventCRTP<Ball, 0> >(this), nr_);
	}
};

class PinMode: public cdevs::State<PinMode>
{
	friend class Pin;
	friend class cereal::access;
public:
	PinMode();
	std::string string() const
	{
		return "Left: " + std::to_string(left_) + "\t\tRight: " + std::to_string(right_);
	}
private:
	unsigned left_;
	unsigned right_;
	std::shared_ptr<Ball> ball_;
	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<PinMode> >(this), left_, right_, ball_);
	}
};

class Pin: public cdevs::Atomic<Pin, PinMode>
{
	friend class GaltonMachine;
	friend class cereal::access;
public:
	Pin(std::string name = "Pin");

	PinMode const& ExtTransition(
	        std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
	                std::owner_less<std::weak_ptr<cdevs::Port> > > inputs);
	PinMode const& IntTransition();

	std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
	        std::owner_less<std::weak_ptr<cdevs::Port> > > OutputFunction();

	double TimeAdvance();

	std::weak_ptr<cdevs::Port> IN;
	std::weak_ptr<cdevs::Port> OUT_LEFT;
	std::weak_ptr<cdevs::Port> OUT_RIGHT;
private:
	std::default_random_engine rand_generator_;
	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<Pin, PinMode>>(this), IN, OUT_LEFT, OUT_RIGHT);
	}
};

class BinMode: public cdevs::State<BinMode>
{
	friend class Bin;
	friend class GaltonMachine;
	friend class cereal::access;
public:
	BinMode()
		: count_(0)
	{
	}
	std::string string() const
	{
		return std::to_string(count_);
	}
private:
	unsigned count_;
	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<BinMode> >(this), count_);
	}
};

class Bin: public cdevs::Atomic<Bin, BinMode>
{
	friend class GaltonMachine;
	friend class cereal::access;
public:
	Bin(unsigned id = 0);
	BinMode const& ExtTransition(
	        std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
	                std::owner_less<std::weak_ptr<cdevs::Port> > > inputs);
	double TimeAdvance();
	std::weak_ptr<cdevs::Port> IN;
private:
	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<Bin, BinMode> >(this));
	}
};

class GeneratorState: public cdevs::State<GeneratorState>
{
	friend class Generator;
	friend class cereal::access;
public:
	GeneratorState(std::string str = "")
		: State(), str_(str), count_(0)
	{
	}
	std::string string() const
	{
		return str_;
	}
protected:
	std::string str_;
	unsigned count_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<GeneratorState> >(this), str_);
	}
};

class Generator: public cdevs::Atomic<Generator, GeneratorState>
{
	friend class cereal::access;
public:
	Generator(unsigned max_count = 100);

	GeneratorState const& IntTransition();
	std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
	        std::owner_less<std::weak_ptr<cdevs::Port> > > OutputFunction();
	double TimeAdvance();
	std::weak_ptr<cdevs::Port> send_event1_;
protected:
	unsigned max_count_;
	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<Generator, GeneratorState>>(this), send_event1_);
	}
};

class GaltonMachine: public cdevs::Coupled<GaltonMachine>
{
	friend class cereal::access;
public:
	GaltonMachine(unsigned nr_trays = 5, unsigned max_count = 100);

	std::shared_ptr<BaseDevs> Select(std::list<std::shared_ptr<BaseDevs>> imm_children) const;
	void print_bins(bool full = false)
	{
		std::string t;
		if (full) {
			for (auto pins : pins_) {
				for (auto pin : pins) {
					t += pin->get_full_name() + ": " + pin->state_->string() + "\n";
				}
				t += "\n";
			}
		}
		for (auto b : bins_) {
			t += b->get_full_name() + ": " + std::to_string(b->state_->count_) + "\n";
		}
		std::cout << t << std::endl;
	}
private:
	std::vector<std::vector<std::shared_ptr<Pin>>>pins_;
	std::vector<std::shared_ptr<Bin>> bins_;
	std::shared_ptr<Generator> generator_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Coupled<GaltonMachine>>(this), pins_, bins_);
		//SetComponentSet( { policeman, trafficlight });
	}
};

}
}

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs_examples::galton::Ball)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::Pin)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::Bin)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::Generator)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::GaltonMachine)

#endif /* EXAMPLES_TRAFFICLIGHT_CLASSIC_MODEL_H_ */
