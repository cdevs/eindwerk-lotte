/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "model.h"
#include "Simulator.h"

#include <sstream>

#include <cereal/archives/binary.hpp>

using namespace cdevs;
using namespace cdevs_examples::galton;
/*
bool TerminateWhenStateIsReached(DevsTime clock, std::shared_ptr<BaseDevs> model)
{
	std::shared_ptr<GaltonMachine> tl = std::dynamic_pointer_cast<GaltonMachine>(model);
	if (tl) {
		return (tl->getState().getValue() == "manual");
	}
	return false;
}*/

int main(int argc, char * argv[]) {
	unsigned def_bins = 5;
	unsigned def_time = 10;
	if(argc == 3){
		def_bins = std::atoi(argv[1]);
		def_time = std::atoi(argv[2]);
	}

	std::shared_ptr<GaltonMachine> model = GaltonMachine::create(def_bins);

	Simulator sim(model);

	// sim.setTerminationCondition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)>(TerminateWhenStateIsReached));

	sim.set_termination_time(def_time);

//	std::shared_ptr<TracerCustom> tracer = std::make_shared<TracerCustom>();
//	sim.set_custom_tracer(tracer);

	sim.set_verbose("classic.txt");
//	sim.set_verbose();
	sim.set_classic_devs();
	sim.set_scheduler_heap();
//	sim.setSchedulerSortedList();
//	sim.setSchedulerList();
	sim.set_classic_devs();
	sim.Simulate();
	model->print_bins();

	return 0;
}
