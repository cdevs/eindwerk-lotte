/*
 * Message.cpp
 *
 *  Created on: Jun 12, 2015
 *      Author: paul
 */


#include "Message.h"

namespace cdevs {

/**
 * \brief Constructor
 *
 * @param timestamp  The timestamp of the message
 */
Message::Message(DevsTime timestamp) : timestamp_(timestamp), destination_(0), color_(kWhite1)
{

}

/**
 * \brief Gets the content of the message
 *
 * @return  The content of the message
 */
const std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
        std::owner_less<std::weak_ptr<Port> > >& Message::get_content() const
{
	return content_;
}

/**
 * \brief Gets the timestamp of the message
 *
 * @return  The timestamp of the message
 */
DevsTime Message::get_timestamp() const
{
	return timestamp_;
}

/**
 * \brief Gets the destination of the message
 *
 * @return  The destination of the message
 */
int Message::get_destination() const
{
	return destination_;
}

/**
 * \brief Smaller-than operator
 *
 * The messages are compared based on the timestamp
 *
 * @return True if the current message's timestamp is smaller than the one compared with,
 * false otherwise
 */
bool Message::operator<(const Message& right) const
{
	return timestamp_ < right.get_timestamp();
}

/**
 * \brief Greater-than operator
 *
 * The messages are compared based on the timestamp
 *
 * @return True if the current message's timestamp is greater than the one compared with,
 * false otherwise
 */
bool Message::operator>(const Message& right) const
{
	return timestamp_ > right.get_timestamp();
}

/**
 * \brief Equals-to operator
 *
 * The messages are compared based on the timestamp
 *
 * @return True if the current message's timestamp is equals to the one compared with,
 * false otherwise
 */
bool Message::operator==(const Message& right) const
{
	return timestamp_ == right.get_timestamp();
}

/**
 * \brief Sets the color (Mattern's algorithm) of the message
 *
 * @param color  The color to set the message to
 */
void Message::set_color(Color color)
{
	color_ = color;
}

/**
 * \brief Gets the color (Mattern's algorithm) of the message
 *
 * @return  The color to set the message to
 */
Color Message::get_color() const
{
	return color_;
}

}
