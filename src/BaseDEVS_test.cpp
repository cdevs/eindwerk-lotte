/*
 * BaseDEVS_test.cpp
 *
 *  Created on: Feb 23, 2015
 *      Author: paul
 */

#include "BaseDEVS.h"
#include "AtomicDevs.h"
#include "gtest/gtest.h"
#include "atomicmodel_test/model.h"
#include "trafficlight_classic/model.h"

namespace cdevs {

TEST(ModelTest, Atomic)
{
	// std::string modelpath = "examples/libAtomicModelTest";
	// std::shared_ptr<BaseDevs> model = BaseDevs::load(modelpath);
	std::shared_ptr<BaseDevs> model = cdevs_examples::atomic::TrafficLight::create("VerkeersLicht");

	ASSERT_TRUE(model != NULL);
	EXPECT_EQ("VerkeersLicht", model->get_model_name());
}

TEST(ModelTest, TrafficLight_Classic)
{
	// std::string modelpath = "examples/libTrafficLight_Classic";
	// std::shared_ptr<BaseDevs> model = BaseDevs::load(modelpath);

	std::shared_ptr<BaseDevs> model = cdevs_examples::atomic::TrafficLight::create("trafficSystem");

	ASSERT_TRUE(model != NULL);
	EXPECT_EQ("trafficSystem", model->get_model_name());
}

} /* namespace ns_DEVS */
