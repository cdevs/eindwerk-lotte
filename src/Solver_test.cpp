#include "Solver.h"
#include "gtest/gtest.h"

namespace cdevs {

TEST(Solver_Test, SetGetClassicDevs){
	Solver s(1);
	s.set_classic_devs(true);
	EXPECT_TRUE(s.get_classic_devs());
	s.set_classic_devs(false);
	EXPECT_FALSE(s.get_classic_devs());
}

} /* namespace ns_DEVS */
