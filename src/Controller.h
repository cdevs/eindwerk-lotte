#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include "BaseSimulator.h"
#include "allocators/Allocator.h"
#include "allocators/AutoAllocator.h"
#include "ThreadEvent.h"
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>

namespace cdevs {
/**
 *  The controller class, which is a special kind of normal simulation kernel. This should always run on the node labeled 0.
 *  It contains some functions that are only required to be ran on a single node, such as GVT initiation
 */
class Controller: public BaseSimulator
{
	friend class Simulator;
public:
	Controller(std::shared_ptr<RootDevs> model, unsigned n_kernels = 1);
	virtual ~Controller();

	std::shared_ptr<BaseSimulator> get_kernel(unsigned index);

	void BroadcastModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models,
	        SchedulerType scheduler_type, bool is_flattened);

	void set_globals(unsigned loglevel, unsigned checkpoint_frequency, std::string checkpoint_name,
	        std::shared_ptr<Tracers> tracers);

	// GVT
	void GvtDone();
	void StartGvtThread(double gvt_interval);

	// simulation
	void SimulateAll();
	void JoinSimulations();
	void set_termination_condition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> termination_condition);
	void SetTerminationTime(DevsTime at_time);
	bool CheckTerminationCondition(DevsTime time, std::shared_ptr<BaseDevs> model);
	bool get_has_termination_condition();

	void FinishAll();
	void WaitFinish();
	void FinishAtTime(DevsTime at_time);

	// tracer
	void RegisterTracer(std::shared_ptr<Tracer> tracer);
	void RunTrace(std::weak_ptr<Tracer> tracer, std::shared_ptr<AtomicDevs> model, std::string text);

	// model
	static std::shared_ptr<Controller> create(std::shared_ptr<RootDevs> model, unsigned n_kernels = 1);
protected:
	Controller();
private:
	// GVT
	void ThreadGvt(int frequency);

	// simulation
	bool IsFinished();

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<BaseSimulator>(this), kernels_, has_termination_condition_, tracers_,
		        should_wait_for_gvt_, should_run_gvt_);
	}

	std::vector<std::shared_ptr<BaseSimulator> > kernels_;

	std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> termination_condition_;
	bool has_termination_condition_; // Set this when changing termination_condition

	std::shared_ptr<Tracers> tracers_;

	std::list<std::thread> kernel_threads_;

	bool should_wait_for_gvt_;
	bool should_run_gvt_;

	std::thread gvt_thread_;

	ThreadEvent wait_for_gvt_;
	ThreadEvent event_gvt_;

	std::mutex no_finish_lock_;

	std::shared_ptr<Allocator> initial_allocator_;

	//Graph event_graph_;
	std::map<int, int> allocations_;

	bool has_graph_;
	bool has_allocations_;
};

}

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs::Controller)

#endif /* CONTROLLER_H_ */
