#include "Port.h"
#include "BaseDEVS.h"

namespace cdevs {

/**
 * \brief Constructor
 *
 * @param is_input  Flag determining whether the port is an input port or an output port
 * @param name  The name for the port
 */
Port::Port(bool is_input, std::string name)
{
	this->is_inport_ = is_input;
	this->name_ = name;
	msg_count_ = 0;
}

/**
 * \brief Gets the port name
 *
 * @return  The name of the port
 */
const std::string Port::get_port_name() const
{
	return this->name_;
}

/**
 * \brief Sets the host model of the port
 *
 * @param model  The host model of the port
 */
void Port::set_host_devs(std::weak_ptr<BaseDevs> model)
{
	host_DEVS_ = model;
}

/**
 * \brief Resets the host model of the port
 */
void Port::ResetHostDEVS()
{
	host_DEVS_.reset();
}

/**
 * \brief Gets the port's message count
 *
 * @return  The message count of the port
 */
unsigned Port::get_msg_count() const
{
	return msg_count_;
}

/**
 * \brief Sets the port's message count
 *
 * @param msg_count  The message count of the port
 */
void Port::set_msg_count(unsigned msg_count)
{
	msg_count_ = msg_count;
}

/**
 * \brief Destructor
 */
Port::~Port()
{
}

/**
 * \brief Smaller-than operator
 *
 * The ports are compared based on the alphabetical order of the name of the port
 *
 * @return True if the name of the current port its alphabetical order is smaller than the one compared with,
 * false otherwise
 */
bool Port::operator<(const Port& b) const
{
	return (name_.compare(b.name_) < 0);
}

/**
 * \brief Equals-to operator
 *
 * The ports are compared based on the name and input flag of the port
 *
 * @return True if the port's name and input flag is the same as the one compared with
 */
bool Port::operator==(const Port& b) const
{
	return (name_.compare(b.name_) == 0) && (is_inport_ == b.is_inport_);
}

} /* namespace ns_DEVS */
