/*
 * BaseDEVS.cpp
 *
 *  Created on: Feb 23, 2015
 *      Author: paul
 */

#include "BaseDEVS.h"
#include <algorithm>
#include <iostream>
#include <cassert>

namespace cdevs {
int BaseDevs::basedevs_counter = 0;

/**
 * \brief The constructor for BaseDevs.
 *
 * @param name The name of this model.
 */
BaseDevs::BaseDevs(std::string name)
	: name_(name), full_name_(name), location_(-1)
{
	elapsed_ = 0.0;
	num_children_ = 0;
	handle_ = 0;

	model_id_ = BaseDevs::basedevs_counter;
	BaseDevs::basedevs_counter++;
	is_coupled_ = false;
}

/*
 * \brief Returns whether or not a structural change is needed.
 *
 * DEFAULT function for Dynamic Structure DEVS, always returning False
 * (thus indicating that no structural change is needed).
 *
 * @param state A state passed from the kernel.
 * @return A boolean indicating whether or not a structural change is necessary.
 */
bool BaseDevs::ModelTransition(const StateBase& state)
{
	return false;
}

/**
 * \brief Changes the parent for this model.
 *
 * @param parent A pointer to the new parent.
 */
void BaseDevs::set_parent(const std::weak_ptr<BaseDevs>& parent)
{
	parent_ = parent;
}


/*
 * \brief Creates a new port and add it everywhere it is necessary.
 *
 * @param name The name of the port to be created.
 * @param isInput Indicates whether this is an input or output port.
 */
std::weak_ptr<Port> BaseDevs::AddPort(std::string name, bool isInput)
{
	std::shared_ptr<Port> port = std::make_shared<Port>(isInput, name);

	if (isInput)
		input_ports_.push_back(port);
	else
		output_ports_.push_back(port);

	// _ports.push_back(port);
	return std::weak_ptr<Port>(port);
}

/*
 * \brief Removes a port and disconnects it from all others.
 *
 * @param port The port to be removed from the model.
 */
void BaseDevs::RemovePort(std::weak_ptr<Port> port)
{
	auto port_sp = port.lock();
	if (port_sp->isInput())
		input_ports_.erase(std::remove(input_ports_.begin(), input_ports_.end(), port_sp), input_ports_.end());
	else
		output_ports_.erase(std::remove(output_ports_.begin(), output_ports_.end(), port_sp),
		        output_ports_.end());
	//_ports.erase(std::remove(_ports.begin(),
	//	_ports.end(), port), _ports.end());
}
/*
 * \brief Adds an inputport to the DEVS model.
 *
 * @param name The name of the port to be created.
 */
std::weak_ptr<Port> BaseDevs::AddInPort(std::string name)
{
	return AddPort(name, true);
}
/*
 * \brief adds out port to the DEVS model.
 *
 * @param name The name of the port to be created.
 */
std::weak_ptr<Port> BaseDevs::AddOutPort(std::string name)
{
	return AddPort(name, false);
}
/*
 * \brief Returns model name
 *
 * @return The name for this model.
 */
const std::string BaseDevs::get_model_name()
{
	return name_;
}
/*
 * Returns the full model name, including path from the root
 *
 * @return The full name for this model.
 */
const std::string BaseDevs::get_model_full_name()
{
	return full_name_;
}

/**
 * Returns the ID of the DEVS model
 *
 * @return The ID of this model.
 */
int BaseDevs::get_model_id() const
{
	return model_id_;
}

/**
 * \brief Sets the ID for this model.
 *
 * @param id The new ID for this model.
 */
void BaseDevs::set_model_id(int id)
{
	model_id_ = id;
}

/**
 * \brief Returns the timestamp for the last transition in this model.
 *
 * @return The timestamp for the last transition in this model.
 */
DevsTime BaseDevs::get_time_last() const
{
	return time_last_;
}

/**
 * \brief Sets the timestamp for the last transition in this model.
 *
 * @param timeLast The timestamp for the last transition.
 */
void BaseDevs::set_time_last(const DevsTime& timeLast)
{
	time_last_ = timeLast;
}

/**
 * \brief Returns the timestamp for the first scheduled transition in this model.
 *
 * @return The timestamp for the first scheduled transition in this model.
 */
DevsTime BaseDevs::get_time_next() const
{
	return time_next_;
}

/**
 * \brief Sets the timestamp for the first scheduled transition in this model.
 *
 * @param timeNext The timestamp for the first scheduled transition in this model.
 */
void BaseDevs::set_time_next(const DevsTime& timeNext)
{
	time_next_ = timeNext;
}

/**
 * \brief The destructor for BaseDevs.
 *
 * This handles the unloading of models loaded as libraries at runtime as well.
 */
BaseDevs::~BaseDevs()
{
	// handle its own unloading (this is the library itself).
	if (handle_ != NULL) {
#if defined(_MSC_VER) // Microsoft compiler
		FreeLibrary((HINSTANCE)handle_);
#elif defined(__GNUC__) // GNU compiler
		dlclose(handle_);
#endif
	}
}

/**
 * \brief Sets the time elapsed since the last transition.
 *
 * This is only a helper method for models and doesn't really affect the simulation.
 * @param timeElapsed The interval in seconds since the last transition.
 */
void BaseDevs::set_time_elapsed(double timeElapsed)
{
	this->elapsed_ = timeElapsed;
}

/**
 * \brief Gets the time elapsed for this DEVS
 *
 * @return Time elapsed.
 */
double BaseDevs::get_time_elapsed()
{
	return elapsed_;
}

/**
 * \brief Flattens the DEVS model
 *
 * Provides a flattened view of the given model,
 */
void BaseDevs::FlattenConnections()
{
}

/**
 * \brief Unlattens the DEVS model
 */
void BaseDevs::UnflattenConnections()
{
}

/**
 * \brief This methods implements support for the equality (==) operator between
 * two instances of BaseDevs.
 *
 * @return true if model id's are equal.
 */
bool BaseDevs::operator==(const BaseDevs& rhs)
{
	return (model_id_ == rhs.model_id_);
}

/**
 * \brief Sets the pointer to a instance of a library loaded by
 * BaseDevs::load(std::string path).
 *
 * @param h The pointer to the library in memory.
 */
void BaseDevs::set_handle(void* h)
{
	handle_ = h;
}

/**
 * \brief Gets the pointer to a instance of a library loaded by
 * BaseDevs::load(std::string path).
 *
 * @return The pointer to the library in memory.
 */
void* BaseDevs::get_handle() const
{
	return handle_;
}

/**
 * \brief Loads an instance of a model from a dynamic library by path.
 *
 * The library should contain a method 'std::shared_ptr<BaseDevs> construct()' with C linkage
 * (extern "C").
 *
 * @param path The path to the library to be loaded.
 * @return A pointer to the instantiated model.
 */
std::shared_ptr<BaseDevs> BaseDevs::Load(std::string path)
{
#if defined(PREDEF_OS_WINDOWS) // Windows
	path += ".dll";
#elif defined(PREDEF_OS_MACOSX) // Mac OS X
	path += ".dylib";
#elif defined(PREDEF_OS_LINUX) // Linux
	path += ".so";
#else
#error Your compiler is not supported
#endif

	// Load the plugin's .so file
	void *handle = NULL;

#if defined(PREDEF_COMPILER_VISUALC) // Microsoft compiler
	if(!(handle = (void*)LoadLibrary(path.c_str())))
	{
		std::cerr << "Model: loading failed. (error code " << GetLastError() << ")" << std::endl;
		return NULL;
	}
#elif defined(PREDEF_COMPILER_GCC) // GNU compiler or Mac OS X
	if (!(handle = dlopen(path.c_str(), RTLD_LAZY))) {
		std::cerr << "Model: " << dlerror() << std::endl;
		return NULL;
	}
	dlerror();
#endif

	// Get the modelConstructor function
	BaseDevs * (*construct)(void);
#if defined(PREDEF_COMPILER_VISUALC) // Microsoft compiler
	construct = (std::shared_ptr<BaseDevs> (*)()) GetProcAddress((HMODULE)handle, "construct");
#elif defined(PREDEF_COMPILER_GCC) // GNU compiler or Mac OS X
	construct = (BaseDevs * (*)(void)) dlsym(handle, "construct");	//TODO: ISO C++ Standard forbids this?
#endif
char	*error = NULL;

#if defined(PREDEF_COMPILER_GCC) // GNU compiler or Mac OS X
	if ((error = dlerror())) {
		std::cerr << "Model: " << dlerror() << std::endl;
		dlclose(handle);
		return NULL;
	}
#endif

	// Construct a model
	std::shared_ptr<BaseDevs> model;
	if (construct != NULL) {
		model = std::shared_ptr<BaseDevs>(construct());
		model->set_handle(handle);
	}
	return model;
}
}
