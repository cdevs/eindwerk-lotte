/*
 * CoupledDEVS.h
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#ifndef COUPLEDDEVS_H_
#define COUPLEDDEVS_H_

#include "BaseDEVS.h"
#include <list>
#include <queue>
#include "AtomicDevs.h"
#include "exceptions/RuntimeDevsException.h"
#include <iostream>

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/memory.hpp>

namespace cdevs {

/**
 * Abstract base class for all coupled-DEVS descriptive classes.
 */
class CoupledDevs: public BaseDevs
{
public:
	CoupledDevs(std::string name);
	void ForceSequential();
	virtual std::shared_ptr<BaseDevs> Select(std::list<std::shared_ptr<BaseDevs>> imm_children) const; //Should have a list of children, and return a child.

	int Finalize(std::string name, int model_counter, std::map<int, std::shared_ptr<BaseDevs>>& model_ids,
	        std::list<std::shared_ptr<BaseDevs>> select_hierarchy);
	void set_location(int location, bool force);

	void FlattenConnections();
	void UnflattenConnections();

	void AddSubModel(std::shared_ptr<BaseDevs> model, int location = 0);

	std::list<std::shared_ptr<BaseDevs> > DirectConnect();

	void DisconnectPorts(std::weak_ptr<Port> port1, std::weak_ptr<Port> port2);
	void ConnectPorts(std::weak_ptr<Port> port1, std::weak_ptr<Port> port2);

	void AddComponent(std::weak_ptr<BaseDevs> component);
	void SetComponentSet(std::list<std::shared_ptr<BaseDevs> > component_set);
	std::list<std::shared_ptr<BaseDevs> > GetComponentSet();

	static std::list<std::shared_ptr<AtomicDevs> > ComponentSetToAtomic(
	        std::list<std::shared_ptr<BaseDevs> > component_set);

	virtual ~CoupledDevs();

	static std::shared_ptr<CoupledDevs> load(std::string path);
	static std::list<std::shared_ptr<BaseDevs> > DirectConnect(std::list<std::shared_ptr<BaseDevs> > componentset);

	void ActuallyConnectPorts();
private:
	std::list<std::pair<std::weak_ptr<Port>, std::weak_ptr<Port> > > ports_to_connect_;
	std::list<std::shared_ptr<BaseDevs> > component_set_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<BaseDevs>(this), ports_to_connect_, component_set_);
	}
};

template<class Derived>
class Coupled: public DevsCRTP<CoupledDevs, Derived>
{
public:
	using DevsCRTP<CoupledDevs, Derived>::DevsCRTP;

	template<typename ... T>
	static std::shared_ptr<Derived> create(T&& ... all)
	{
		std::shared_ptr<Derived> model = DevsCRTP<BaseDevs, Derived>::create(std::forward<T>(all)...);

		std::shared_ptr<CoupledDevs> coupled = std::static_pointer_cast<CoupledDevs>(model);

		for (auto& m : coupled->GetComponentSet())
			m->set_parent(model->getPtr());

		coupled->ActuallyConnectPorts();

		return model;
	}
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<DevsCRTP<CoupledDevs, Derived>>(this));
	}
};

std::function<Event(Event)> AppendZ(std::function<Event(Event)> first_z, std::function<Event(Event)> new_z);

} /* namespace ns_DEVS */

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs::CoupledDevs)

#endif /* COUPLEDDEVS_H_ */
