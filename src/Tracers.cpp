#include "Tracers.h"

namespace cdevs {
/**
 * \brief Constructor
 */
Tracers::Tracers()
{
	uid_ = 0;
}

/**
 * \brief Destructor
 */
Tracers::~Tracers()
{
}

/**
 * \brief Registers the given tracers
 *
 * @param tracer  The tracer to register
 */
void Tracers::RegisterTracer(std::shared_ptr<Tracer> tracer)
{
	this->tracers_.push_back(tracer);
	this->uid_++;
}

/**
 * \brief Checks if the Tracers object has registered tracers
 *
 * @return  True if the Tracers object has registered tracers, false if it doesn't
 */
bool Tracers::HasTracers() const
{
	return this->tracers_.size() > 0;
}

/**
 * \brief Gets a tracer by ID
 *
 * @param uid  The ID of the tracers
 * @return  A shared pointer to the tracer
 */
std::shared_ptr<Tracer> Tracers::GetById(int uid) const
{
	return this->tracers_[uid];
}

/**
 * \brief Gets all registered tracers
 *
 * @return  All registered tracers
 */
std::vector<std::shared_ptr<Tracer> > Tracers::GetTracers() const
{
	return tracers_;
}

/**
 * \brief Stops all tracers
 */
void Tracers::StopTracers()
{
	for (auto& tracer : this->tracers_) {
		tracer->StopTracer();
	}
}

/**
 * \brief Calls the tracers to perform the initialize trace for the given
 * atomic model at the given time
 *
 * @param devs  The model to perform the initialize trace for
 * @param t  The time of calling the initialize trace
 */
void Tracers::TracesInitialize(std::shared_ptr<AtomicDevs> devs, DevsTime t)
{
	for (auto& tracer : this->tracers_) {
		tracer->TraceInitialize(devs, t);
	}
}

/**
 * \brief Calls the tracers to perform the internal trace for the given
 * atomic model
 *
 * @param devs  The model to perform the internal trace for
 */
void Tracers::TracesInternal(std::shared_ptr<AtomicDevs> devs)
{
	for (auto& tracer : this->tracers_) {
		tracer->TraceInternal(devs);
	}
}

/**
 * \brief Calls the tracers to perform the external trace for the given
 * atomic model
 *
 * @param devs  The model to perform the external trace for
 */
void Tracers::TracesExternal(std::shared_ptr<AtomicDevs> devs)
{
	for (auto& tracer : this->tracers_) {
		tracer->TraceExternal(devs);
	}
}

/**
 * \brief Calls the tracers to perform the confluent trace for the given
 * atomic model
 *
 * @param devs  The model to perform the confluent trace for
 */
void Tracers::TracesConfluent(std::shared_ptr<AtomicDevs> devs)
{
	for (auto& tracer : this->tracers_) {
		tracer->TraceConfluent(devs);
	}
}

/**
 * \brief Resets the Tracers object by clearing all registered tracers
 */
void Tracers::Reset()
{
	tracers_.clear();
}

/**
 * \brief Starts all tracers
 *
 * @param recover  Flag determining whether this called from a recovery phase
 */
void Tracers::StartTracers(bool recover)
{
	for (auto tracer : tracers_) {
		tracer->StartTracer(recover);
	}
}

} /* namespace cdevs */
