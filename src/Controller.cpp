#include "Controller.h"

namespace cdevs {
/**
 * \brief Constructor
 *
 * @param model  Model to host at the kernel
 * @param n_kernels  The amount of kernels to use for simulation
 */
Controller::Controller(std::shared_ptr<RootDevs> model, unsigned n_kernels)
	: BaseSimulator(0, model), tracers_(std::make_shared<Tracers>())
{
	should_wait_for_gvt_ = false;
	should_run_gvt_ = false;

	has_allocations_ = false;
	has_graph_ = false;

	termination_condition_ = [](DevsTime time, std::shared_ptr<BaseDevs> model) -> bool {return false;};
	has_termination_condition_ = false;
	n_kernels_ = n_kernels;
}

/**
 * \brief Default constructor (private, used for serializing).
 */
Controller::Controller()
	: has_termination_condition_(), should_wait_for_gvt_(), should_run_gvt_(), has_graph_(), has_allocations_()
{
}

/**
 * \brief Destructor
 */
Controller::~Controller()
{

}

/**
 * \brief Gets the kernel for the given index
 *
 * @return  A shared pointer to the kernel at the given index
 */
std::shared_ptr<BaseSimulator> Controller::get_kernel(unsigned index)
{
	return kernels_.at(index);
}

/**
 * \brief Broadcasts a model to each kernel
 *
 * @param model  The model to broadcast
 * @param models  List containing all models
 * @param scheduler_type  The type of the scheduler to use for the model
 */
void Controller::BroadcastModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models,
        SchedulerType scheduler_type, bool is_flattened)
{
	AutoAllocator allocator;
	allocator.Allocate(models, n_kernels_);
	for (auto& kernel : kernels_)
		kernel->SendModel(model, models, scheduler_type, is_flattened);
}

/**
 * \brief Start the simulation on all kernels concurrently.
 */
void Controller::SimulateAll()
{
	for (auto& kernel : kernels_) {
		kernel_threads_.push_back(std::thread(&BaseSimulator::Simulate, kernel));
	}
}

/**
 * \brief Joins all simulation threads to the main thread
 */
void Controller::JoinSimulations()
{
	for (auto& thread : kernel_threads_)
		thread.join();
}

/**
 * \brief Configure all 'global' variables for all kernel.
 *
 * @param loglevel Level of logging library.
 * @param checkpoint_frequency Frequency at which checkpoints should be made.
 * @param checkpoint_name Name of the checkpoint to save.
 * @param statesaver The instance of StateSaver to use.
 */
void Controller::set_globals(unsigned loglevel, unsigned checkpoint_frequency, std::string checkpoint_name,
        std::shared_ptr<Tracers> tracers)
{
	for (auto& kernel : kernels_)
		kernel->set_globals(loglevel, checkpoint_frequency, checkpoint_name, kernels_.size(), tracers);
}

/**
 * \brief Notify this simulation kernel that the GVT calculation is finished
 */
void Controller::GvtDone()
{
	wait_for_gvt_.Set();
}

/**
 * \brief Start the GVT thread
 *
 * @param gvt_interval the interval between two successive GVT runs
 */
void Controller::StartGvtThread(double gvt_interval)
{
	should_run_gvt_ = true;
	gvt_thread_ = std::thread(&Controller::ThreadGvt, this, gvt_interval);
}

/**
 * \brief Run the GVT algorithm, this method should be called in its own thread,
 * because it will block
 *
 * @param frequency: the time to sleep between two GVT calculations in ms
 */
void Controller::ThreadGvt(int frequency)
{
	// pause for a brief moment before looping
	event_gvt_.Wait(frequency);

	while (should_run_gvt_) {
		// setup initial GVT control message
		GvtControlMessage message(std::numeric_limits<double>::infinity(),
		        std::numeric_limits<double>::infinity(), accumulator_, { });

		// execute GVT calculations, starting from initial GVT control message
		ReceiveControl(message, true);

		// wait for lock
		wait_for_gvt_.Wait();
		wait_for_gvt_.Clear();

		// Limit the GVT algorithm, otherwise this will flood the ring
		event_gvt_.Wait(frequency);
	}
}


/**
 * Sets the termination condition of this simulation kernel.
 * As soon as the condition is valid, it will signal all nodes that they have to stop simulation as soon as they have progressed up to this simulation time.
 * @param termination_condition: a function that accepts two parameters: *time* and *model*. Function returns whether or not to halt simulation
 */
void Controller::set_termination_condition(
        std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> termination_condition)
{
	termination_condition_ = termination_condition;
	termination_time_check_ = false;
	has_termination_condition_ = true;
}

/**
 * \brief Sets the time at which simulation should stop, setting this will override
 * the local simulation condition for all kernels.
 *
 * @param time: the time at which the simulation should stop
 */
void Controller::SetTerminationTime(DevsTime at_time)
{
	for (auto& kernel : kernels_)
		kernel->set_termination_time(at_time);
}

/**
 * \brief Registers a tracer
 *
 * Registers the tracers at each kernel
 *
 * @param tracer  The tracer to register
 */
void Controller::RegisterTracer(std::shared_ptr<Tracer> tracer)
{
	for (auto& kernel : kernels_)
		kernel->RegisterTracer(tracer);
}

/**
 * \brief Runs a trace
 *
 * @param tracer  The tracer to use
 * @param model The model invoking the trace
 * @param text  The text to be printend
 */
void Controller::RunTrace(std::weak_ptr<Tracer> tracer, std::shared_ptr<AtomicDevs> model, std::string text)
{
	//workaround, for some reason test fail when calling delayedaction for 1 kernel
	DelayedAction(tracer, model->get_time_last(), model, text);
}

/**
 * \brief Creates a controller
 *
 * @param model  The model hosted on the controller
 * @param n_kernels  The amount of kernels to run the simulation on
 * @return  A shared pointer to the created controller
 */
std::shared_ptr<Controller> Controller::create(std::shared_ptr<RootDevs> model, unsigned n_kernels)
{
	auto controller = std::make_shared<Controller>(model, n_kernels);

	controller->kernels_.clear();
	controller->set_controller(controller);
	controller->kernels_.push_back(controller);
	for (unsigned i = 1; i < n_kernels; i++) {
		controller->kernels_.push_back(std::make_shared<BaseSimulator>(i, model));
		controller->kernels_.back()->set_controller(controller);
	}

	return controller;
}

/**
 * \brief Stop the currently running simulation
 */
void Controller::FinishAll()
{
	for (auto& kernel : kernels_) {
		kernel->Finish();
	}
}

/**
 * \brief Wait until the specified number of kernels have all told that simulation
 * finished.
 */
void Controller::WaitFinish()
{
	while (true) {
		// sleep for a while
		std::this_thread::sleep_for(std::chrono::milliseconds(1));

		// check if we can finish
		if (IsFinished())
			break;
	}

	// stop the GVT
	should_run_gvt_ = false;

	event_gvt_.Set();

	// join the GVT thread
	if (gvt_thread_.joinable())
		gvt_thread_.join();
}

/**
 * \brief Checks if all kernels have indicated that they have finished simulation.
 * If each kernel has indicated this, a final (expensive) check happens to
 * prevent premature termination.
 *
 * @param running The number of kernels that is simulating
 * @return Whether or not simulation is already finished
 */
bool Controller::IsFinished()
{
	// lock the finish lock
	no_finish_lock_.lock();

	// get message count by starting initial FinishRing call
	// each kernel will be asked if we may finish
	int msg_count = FinishRing(0, 0, true);

	if (msg_count == -1) {
		// we can not finish

		// release lock
		no_finish_lock_.unlock();
		return false;
	}

	// ask again and see if the amount of messages sent and received are equal
	int msg_count2 = FinishRing(0, 0, true);

	// if they are equal, we are done
	bool done = msg_count == msg_count2;

	if (!done)
		no_finish_lock_.unlock();

	return done;
}

/**
 * \brief Signal this kernel that it may stop at the provided time
 *
 * @param clock: the time to stop at
 */
void Controller::FinishAtTime(DevsTime at_time)
{
	for (auto& kernel : kernels_) {
		kernel->set_termination_time(at_time);
	}
}

/**
 * \brief Checks the termination condition
 *
 * @return True if the termination condition passed, false if it didn't
 */
bool Controller::CheckTerminationCondition(DevsTime time, std::shared_ptr<BaseDevs> model)
{
	return termination_condition_(time, model);
}

/**
 * \brief Checks if the controller has a termination condition
 *
 * @return True if the controller has a termination condition
 */
bool Controller::get_has_termination_condition()
{
	return has_termination_condition_;
}
}

