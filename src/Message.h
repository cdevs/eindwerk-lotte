#ifndef MESSAGE_H_
#define MESSAGE_H_

#include "utility.h"
#include "Port.h"
#include "Color.h"

#include <cereal/access.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/map.hpp>

namespace cdevs {
class Message
{
public:
	Message(DevsTime timestamp);
	Message(DevsTime timestamp, int destination,
	        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	                std::owner_less<std::weak_ptr<Port> > > content, Color color);
	const std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > >& get_content() const;
	DevsTime get_timestamp() const;
	int get_destination() const;
	bool operator<(const Message& right) const;
	bool operator>(const Message& right) const;
	bool operator==(const Message& right) const;
	void set_color(Color color);
	Color get_color() const;
private:

	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > content_;
	DevsTime timestamp_;
	int destination_;

	Color color_;

	Message()
		: destination_(0), color_(kWhite1)
	{
	}

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(timestamp_, destination_, content_, color_);
	}
};

/**
 * \brief Constructor
 *
 * @param timestamp  The timestamp of the message
 * @param destination  The destination model of the message
 * @param content  The content of the message (a mapping between the port and the content)
 * @param color  The color of the message (Mattern's algorithm)
 */
inline Message::Message(DevsTime timestamp, int destination,
        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
                std::owner_less<std::weak_ptr<Port> > > content, Color color)
	: content_(content), timestamp_(timestamp), destination_(destination), color_(color)
{

}

}

#endif
