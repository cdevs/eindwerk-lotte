#include "ClassicDEVSWrapper.h"

namespace cdevs {
/**
 * \brief Constructor
 *
 * @param model The model to wrap around
 */
ClassicDEVSWrapper::ClassicDEVSWrapper(std::shared_ptr<AtomicDevs> model)
	: model_(model)
{
}

/**
 * Wrap around the outputFnc function by changing the returned dictionary
 *
 * @return the changed dictionary
 */
std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >, std::owner_less<std::weak_ptr<Port> > > ClassicDEVSWrapper::OutputFunc()
{
	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > retval = model_->OutputFunction();
	return retval;
}

ClassicDEVSWrapper::~ClassicDEVSWrapper()
{
}
}
