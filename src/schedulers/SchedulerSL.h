/*
 * SchedulerSL.h
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#ifndef SCHEDULERSL_H_
#define SCHEDULERSL_H_

#include "../templates/Scheduler.h"
#include "../AtomicDevs.h"
#include <vector>
#include <cmath>
#include <memory>
#include <algorithm>

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/polymorphic.hpp>

namespace cdevs {

/**
 * The Sorted List scheduler is the simplest scheduler available, though it has
 * extremely bad performance in several situations.
 * It simply keeps a list of all models, which is sorted on time_next. No
 * operations have any influence on this list itself, as there is no real
 * internal representation. As soon as the imminent models are requested,
 * this list is sorted again and the first elements are returned.
 */
class SchedulerSL: public Scheduler
{
public:
	SchedulerSL(std::list<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels);
	void Schedule(std::shared_ptr<AtomicDevs> model);
	void Unschedule(std::shared_ptr<AtomicDevs> model);
	void MassReschedule(std::list<std::shared_ptr<AtomicDevs>> &reschedule_set);
	DevsTime ReadFirst();
	std::list<std::shared_ptr<AtomicDevs> > GetImminent(DevsTime time);
	double get_epsilon();
	int get_total_models();
	virtual ~SchedulerSL();
private:
	SchedulerSL();

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<Scheduler>(this), models_, minval_, epsilon_, totalmodels_);
	}

	std::list<std::shared_ptr<AtomicDevs> > models_;
	DevsTime minval_;
	double epsilon_;
	int totalmodels_;
};

} /* namespace ns_DEVS */

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs::SchedulerSL)

#endif /* SCHEDULERSL_H_ */
