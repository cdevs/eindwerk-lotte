/*
 * SchedulerSL_test.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: Nathan
 */

#include "SchedulerSL.h"
#include "gtest/gtest.h"
#include "atomicmodel_test/model.h"

class SchedulerSL_test: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
	}
	virtual void TearDown()
	{
		// Code here will be called immediately after each test
		// (right before the destructor).
	}

};

TEST_F(SchedulerSL_test,Constructor_test)
{
	std::shared_ptr<cdevs::AtomicDevs> b1 = cdevs_examples::atomic::TrafficLight::create("Model1");
	std::shared_ptr<cdevs::AtomicDevs> b2 = cdevs_examples::atomic::TrafficLight::create("Model2");
	std::shared_ptr<cdevs::AtomicDevs> b3 = cdevs_examples::atomic::TrafficLight::create("Model3");

	std::list<std::shared_ptr<cdevs::AtomicDevs> > m = { b1, b2, b3 };

	cdevs::SchedulerSL myscheduler(m, 0.001, 42);

	EXPECT_NEAR(myscheduler.get_epsilon(), 0.001, 0.0005); //FP Error?
	EXPECT_EQ(myscheduler.get_total_models(), 42);
}

TEST_F(SchedulerSL_test, Test_Schedule)
{
	std::shared_ptr<cdevs::AtomicDevs> b1 = cdevs_examples::atomic::TrafficLight::create("Model1");
	std::shared_ptr<cdevs::AtomicDevs> b2 = cdevs_examples::atomic::TrafficLight::create("Model2");
	std::shared_ptr<cdevs::AtomicDevs> b3 = cdevs_examples::atomic::TrafficLight::create("Model3");
	std::shared_ptr<cdevs::AtomicDevs> b4 = cdevs_examples::atomic::TrafficLight::create("Model4");
	cdevs::DevsTime time1(1, 0), time2(2, 0), time3(3, 0), time4(0.5, 0);
	b1->set_time_next(time1);
	b2->set_time_next(time2);
	b3->set_time_next(time3);
	b4->set_time_next(time4);
	std::list<std::shared_ptr<cdevs::AtomicDevs> > m = { b1, b2, b3 };
	cdevs::SchedulerSL myscheduler(m, 0.001, 42);
	myscheduler.Schedule(b4);
	EXPECT_EQ(myscheduler.ReadFirst(), time4);

}
TEST_F(SchedulerSL_test, Test_UnSchedule)
{
	std::shared_ptr<cdevs::AtomicDevs> b1 = cdevs_examples::atomic::TrafficLight::create("Model1");
	std::shared_ptr<cdevs::AtomicDevs> b2 = cdevs_examples::atomic::TrafficLight::create("Model2");
	std::shared_ptr<cdevs::AtomicDevs> b3 = cdevs_examples::atomic::TrafficLight::create("Model3");
	std::shared_ptr<cdevs::AtomicDevs> b4 = cdevs_examples::atomic::TrafficLight::create("Model4");
	cdevs::DevsTime time1(100, 0), time2(200, 0), time3(300, 0), time4(50, 0);
	b1->set_time_next(time1);
	b2->set_time_next(time2);
	b3->set_time_next(time3);
	b4->set_time_next(time4);
	std::list<std::shared_ptr<cdevs::AtomicDevs> > m = { b1, b2, b3, b4 };
	cdevs::SchedulerSL myscheduler(m, 0.001, 42);
	myscheduler.Unschedule(b4);
	EXPECT_EQ(myscheduler.ReadFirst().get_x(), time1.get_x());

}

TEST_F(SchedulerSL_test, ReadFirst_MassReschedule_test)
{
	std::shared_ptr<cdevs::AtomicDevs> b1 = cdevs_examples::atomic::TrafficLight::create("Model1");
	std::shared_ptr<cdevs::AtomicDevs> b2 = cdevs_examples::atomic::TrafficLight::create("Model2");
	std::shared_ptr<cdevs::AtomicDevs> b3 = cdevs_examples::atomic::TrafficLight::create("Model3");
	cdevs::DevsTime time1(1, 0), time2(2, 0), time3(3, 0);
	b1->set_time_next(time1);
	b2->set_time_next(time2);
	b3->set_time_next(time3);
	std::list<std::shared_ptr<cdevs::AtomicDevs> > m = { b1, b2, b3 };
	cdevs::SchedulerSL myscheduler(m, 0.001, 42);
	myscheduler.MassReschedule(m);
	EXPECT_EQ(myscheduler.ReadFirst(), time1);
}

TEST_F(SchedulerSL_test, GetImminent_test){
	std::shared_ptr<cdevs::AtomicDevs> b1 = cdevs_examples::atomic::TrafficLight::create("Model1");
	std::shared_ptr<cdevs::AtomicDevs> b2 = cdevs_examples::atomic::TrafficLight::create("Model2");
	std::shared_ptr<cdevs::AtomicDevs> b3 = cdevs_examples::atomic::TrafficLight::create("Model3");
	std::shared_ptr<cdevs::AtomicDevs> b4 = cdevs_examples::atomic::TrafficLight::create("Model4");
	cdevs::DevsTime time1(100, 0), time2(200, 0), time3(300, 0), time4(50, 0);
	b1->set_time_next(time1);
	b2->set_time_next(time2);
	b3->set_time_next(time3);
	b4->set_time_next(time4);
	std::list<std::shared_ptr<cdevs::AtomicDevs> > m = { b1, b2, b3, b4 };
	cdevs::SchedulerSL myscheduler(m, 0.001, 42);
	auto p = myscheduler.GetImminent(time4);
	EXPECT_EQ(p.size(), 1);
	EXPECT_EQ(p.front()->get_time_next(), time4);

}
