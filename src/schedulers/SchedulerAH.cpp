/*
 * SchedulerAH.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: Nathan
 */

#include "SchedulerAH.h"

namespace cdevs {
/*
 * \brief Shows a nice overview of the Heapstructure of the Scheduler.
 */

void SchedulerAH::PrettyPrint()
{
	int i = 1;
	std::cout << std::endl << std::endl;
	std::cout << "These are the contents of the heap:" << std::endl;
	for (auto m : this->heap_) {
		std::cout << "Element at position [" << i << "]:" << std::endl;
		std::cout << "TimeNext Element: [Time=" << std::get<0>(m).get_x() << "][Ptr=" << std::get<1>(m).get()
		        << "]" << std::endl;
		i++;
	}
}
/*
 * \brief Constructs the Heap Scheduler. We receive a list of all models, and determine whether to place them on
 * the heap (When they will fire at time X < INF), or discard them (When they will surely never fire X = inf). Models that
 * Have their time_next_ changed can be rescheduled using the Schedule method.
 *
 * @param: models : Models to schedule
 * @param: epsilon : Time between transitions to ignore
 * @param: totalModels : Total number of models (Useful for load balancing, not used here.)
 *
 */
SchedulerAH::SchedulerAH(std::list<std::shared_ptr<AtomicDevs> > &models, float epsilon, unsigned int totalModels)
{
	for (auto m : models) {
		if (m->get_time_next().get_x() != std::numeric_limits<double>::infinity()) {
			//We have a model ready to schedule, as it will certainly fire, because we are not at infinity.
			heap_.push_back(std::make_tuple(m->get_time_next(), m));
			//std::push_heap(heap_.begin(), heap_.end(),Comp());
		} else {
			//Model is not being Scheduled, as it will never fire.
			std::cout << "Constructor : Not scheduling the model!" << std::endl;
		}

	}
	this->epsilon_ = epsilon;
	this->totalModels_ = totalModels;
	std::make_heap(heap_.begin(), heap_.end(), Comp());
	//std::sort_heap(heap_.begin(), heap_.end(), Comp());
}

/*
 * \brief Default constructor for the Heap Scheduler.
 */
SchedulerAH::SchedulerAH() : heap_(), epsilon_(), totalModels_()
{
}

/*
 * \brief Schedules a model. Several checks are performed to make sure we don't do any unneccesary work.
 *
 * @param model: the model to be scheduled
 */
void SchedulerAH::Schedule(std::shared_ptr<AtomicDevs> model)
{
	if (model->get_time_next().get_x() != std::numeric_limits<double>::infinity()) {
		//We check whether the model is already scheduled, by looking at the heap.
		//If it already exists, we update the time, else we push it back.
		auto const& p = std::find_if(heap_.begin(), heap_.end(),
		        [&] (std::tuple<DevsTime, std::shared_ptr<AtomicDevs>> const& t)
		        {
			        return std::get<1>(t) == model;
		        });
		if (p != heap_.end()) {
			//Our model is already in the heap, updating it.
			std::get<0>(*p) = model->get_time_next();
			std::sort_heap(heap_.begin(), heap_.end(), Comp());
			//Change the time, and make sure we maintain the heap.
		} else {
			//We have a new model, Scheduling it...
			heap_.push_back(std::make_tuple(model->get_time_next(), model));
			std::push_heap(heap_.begin(), heap_.end(), Comp());
			std::sort_heap(heap_.begin(), heap_.end(), Comp());
		}
	}

}
/*
 * \brief This method is used to remove a model from the heap.
 * This can be done when a models timenext has become infinite, or when we just want it removed.
 *
 * @param model: The model to unschedule.
 */
void SchedulerAH::Unschedule(std::shared_ptr<AtomicDevs> model)
{
	auto const& p = std::find_if(heap_.begin(), heap_.end(),
	        [&] (std::tuple<DevsTime, std::shared_ptr<AtomicDevs>> const& t)
	        {
		        return std::get<1>(t) == model;
	        });
	if (p != heap_.end()) {
		//We found the element, we will now remove it, and maintain the heap.
		heap_.erase(p);
		std::sort_heap(heap_.begin(), heap_.end(), Comp());
	}

}

/*
 * \brief Given a set of models to reschedule, performs Unschedule(m), Schedule(m). We chose to just do this
 * to avoid code dupe, and doing it the other way really does not improve performance all that much.
 *
 * @param reschedule_set: List of models to reschedule, they do not have to be present in the scheduler.
 */
void SchedulerAH::MassReschedule(std::list<std::shared_ptr<AtomicDevs> > &reschedule_set)
{
	for (auto m : reschedule_set) {
		Unschedule(m);
		Schedule(m);
	}
}
/*
 * \brief Returns the timenext of the element with the lowest time next. C++11 std::x_heap implementation implements a heap
 * of which you can create a min/max heap by choosing which element to return. Our implementation clearly requires
 * a minheap.
 *
 * @return DevsTime being the lowest time_next_
 */
DevsTime SchedulerAH::ReadFirst()
{
	std::sort_heap(heap_.begin(), heap_.end(), Comp());
	return std::get<0>(heap_.back());
}

/*
 * \brief This method returns all imminent Models.
 *
 * @param time: the time before the imminents will fire with error epsilon
 * @return : Returns the list of all atomics ready to fire.
 */
std::list<std::shared_ptr<AtomicDevs> > SchedulerAH::GetImminent(DevsTime time)
{
	double x = time.get_x();
	double y = time.get_y();
	std::list<std::shared_ptr<AtomicDevs> > b;

	if (heap_.empty()) {
		return b;
	}

	auto first = heap_.back(); //Take the element with the lowest TimeNext -> Next to fire.

	while ((std::fabs(std::get<0>(first).get_x() - x) < this->epsilon_) && (std::get<0>(first).get_y() == y)) {
		b.push_back(std::get<1>(first));
		heap_.pop_back();
		std::sort_heap(heap_.begin(), heap_.end(), Comp());
		if (heap_.empty())
			break;
		else
			first = heap_.back();
	}
	return b;
}

/**
 * \brief Destructor
 */
SchedulerAH::~SchedulerAH()
{

}

/**
 * \brief Gets the epsilon value of the scheduler
 *
 * @return  The epsilon value of the scheduler
 */
double SchedulerAH::get_epsilon() const
{
	return epsilon_;
}

/**
 * \brief Gets the total amount of models of the scheduler
 *
 * @return  The total amount of models of the scheduler
 */
int SchedulerAH::get_total_models() const
{
	return totalModels_;
}

} /* namespace ns_DEVS */
