/*
 * SchedulerML.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "SchedulerML.h"
#include <iostream>
#include <limits>

namespace cdevs {

/*
 * \brief Creates the Scheduler, assigning all models and parameters.
 *
 * @param models: List of models for the Scheduler.
 * @param epsilon: Time to wait between current time for imminent.
 * @param totalModels: Number of models.
 */
SchedulerML::SchedulerML(std::list<std::shared_ptr<AtomicDevs> > models, float epsilon, unsigned int totalModels)
	: models_(models), minval_(DevsTime(std::numeric_limits<double>::infinity(), 0)), epsilon_(epsilon), totalmodels_(
	        totalModels)
{
	//calculate a correct minval
	for (auto m : this->models_) {
		if (m->get_time_next() < this->minval_) {
			this->minval_ = m->get_time_next();
		}
	}
}

/*
 * \brief Creates the Scheduler, assigning all models and parameters.
 */
SchedulerML::SchedulerML()
	: models_(), minval_(), epsilon_(), totalmodels_()
{
}

SchedulerML::~SchedulerML()
{

}
/*
 * \brief Schedules a model. We check whether the model is already present.
 * if so, don't schedule it.
 * @param model: Model to be scheduled.
 */
void SchedulerML::Schedule(std::shared_ptr<AtomicDevs> model)
{
	auto p = std::find(models_.begin(), models_.end(), model);
	if (p == models_.end()) {
		models_.push_back(model);
		if (model->get_time_next() < this->minval_) {
			this->minval_ = model->get_time_next();
		}
	}

}
/*
 * \brief Unschedules the model, removes it from the scheduler if it is found, otherwise ignored.
 * Because we don't use a sorted list, we must update our new minimum value by resetting the value
 * and iterating over the list to find the correct new minval.
 *
 * @param model: Model to unschedule.
 */
void SchedulerML::Unschedule(std::shared_ptr<AtomicDevs> model)
{
	std::list<std::shared_ptr<AtomicDevs> >::iterator i = models_.begin();
	while (i != models_.end()) {
		if (*i == model) {
			i = models_.erase(i); //reseat iterator
		} else {
			++i;
		}
	}
	if (model->get_time_next() == this->minval_) {
		this->minval_.set_x(std::numeric_limits<double>::infinity());
		this->minval_.set_y(std::numeric_limits<double>::infinity());
		for (auto& m : models_) {
			if (m->get_time_next() < this->minval_) {
				this->minval_ = m->get_time_next();
			}
		}
	}
}
/*
 * \brief Adds all models not already in the schedulers models to it, and then updates the minval.
 */
void SchedulerML::MassReschedule(std::list<std::shared_ptr<AtomicDevs> > &reschedule_set)
{
	for (auto m : reschedule_set) {
		Schedule(m);
	}

	this->minval_.set_x(std::numeric_limits<double>::infinity());
	this->minval_.set_y(std::numeric_limits<double>::infinity());
	for (auto& m : models_) {
		if (m->get_time_next() < this->minval_) {
			this->minval_ = m->get_time_next();
		}
	}

}
/*
 * \brief Returns the smalles element, which we have conveniently saved.
 *
 * \return The time at which the first transition is to occur
 */
DevsTime SchedulerML::ReadFirst()
{
	return this->minval_;
}
/*
 * \brief returns all imminent models.
 *
 * @param time: All models with a transition before this time, with error epsilon are returned.
 *
 */
std::list<std::shared_ptr<AtomicDevs> > SchedulerML::GetImminent(DevsTime time)
{
	std::list<std::shared_ptr<AtomicDevs> > b;
	for (auto& m : models_) {
		if ((fabs(m->get_time_next().get_x() - time.get_x()) < this->epsilon_)
		        && m->get_time_next().get_y() == time.get_y()) {
			b.push_back(m);
		}
	}
	return b;
}

/**
 * \brief Gets the epsilon value of the scheduler
 *
 * @return  The epsilon value of the scheduler
 */
double SchedulerML::get_epsilon() const
{
	return epsilon_;
}

/**
 * \brief Sets the epsilon value of the scheduler
 *
 * @param epsilon  The epsilon value of the scheduler
 */
void SchedulerML::set_epsilon(double epsilon)
{
	epsilon_ = epsilon;
}

/**
 * \brief Gets the total amount of models of the scheduler
 *
 * @return  The total amount of models of the scheduler
 */
int SchedulerML::get_total_models() const
{
	return totalmodels_;
}

/**
 * \brief Sets the total amount of models of the scheduler
 *
 * @param  The total amount of models of the scheduler
 */
void SchedulerML::set_total_models(int totalmodels)
{
	totalmodels_ = totalmodels;
}

}
