#include "gtest/gtest.h"
#include "Simulator.h"
#include "trafficlight_classic/model.h"

namespace cdevs {

class ThreadEventTest: public ::testing::Test
{
public:
	ThreadEvent thread_event_;
};

TEST_F(ThreadEventTest, Constructor)
{
	ThreadEvent test_thread_event;
	EXPECT_FALSE(test_thread_event.isSet());
}

TEST_F(ThreadEventTest, Set)
{
	EXPECT_FALSE(thread_event_.isSet());
	thread_event_.Set();
	EXPECT_TRUE(thread_event_.isSet());
}

TEST_F(ThreadEventTest, Clear)
{
	thread_event_.Set();
	thread_event_.Clear();
	EXPECT_FALSE(thread_event_.isSet());
}

TEST_F(ThreadEventTest, Wait)
{
	thread_event_.Set();
	thread_event_.Wait();
	EXPECT_TRUE(thread_event_.isSet());
}

}
