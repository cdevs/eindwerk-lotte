#ifndef SOLVER_H_
#define SOLVER_H_

#include "AtomicDevs.h"
#include "RootDEVS.h"
#include "utility.h"
#include "ClassicDEVSWrapper.h"
#include "Message.h"
#include "Color.h"
#include "ThreadEvent.h"
#include <map>
#include <cfloat>
#include <array>
#include <condition_variable>
#include <mutex>
#include <list>
#include "Tracers.h"
#include "exceptions/RuntimeDevsException.h"

#include <cereal/access.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/memory.hpp>

namespace cdevs {

class Controller;
/**
 * A unified DEVS solver, containing all necessary functions
 */
class Solver
{
	friend class Simulator;
public:
	Solver(unsigned id);
	virtual ~Solver();

	void set_controller(std::weak_ptr<Controller> controller);

	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > AtomicOutputGenerationEventTracing(
	        std::shared_ptr<AtomicDevs> adevs, DevsTime time);

	void RegisterTracer(std::shared_ptr<Tracer> tracer);

	void set_classic_devs(bool classic_devs);
	bool get_classic_devs() const;

	unsigned get_id();
protected:
	Solver();
	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > AtomicOutputGeneration(std::shared_ptr<AtomicDevs> adevs,
	        DevsTime time);
	void MassAtomicTransitions(std::map<int, int> trans, DevsTime clock);
	void AtomicInitialize(std::shared_ptr<AtomicDevs> adevs, DevsTime at_time);

	std::shared_ptr<AtomicDevs> FindDevsModelById(int id);

	std::list<std::shared_ptr<AtomicDevs> > CoupledOutputGenerationClassic(DevsTime at_time);
	std::list<std::shared_ptr<AtomicDevs> > CoupledOutputGeneration(DevsTime at_time);
	void CoupledInitialize();
	void NotifySend(int destination, DevsTime timestamp, Color color);
	void NotifyReceive(Color color);

	std::weak_ptr<Controller> controller_;

	int id_; // name of the base simulator
	std::map<int, int> transitioning_;

	static std::mutex temp_mutex_;
	int msg_copy_;

	bool has_initial_allocator_;
	bool do_some_tracing_;
	bool is_simulation_irreversible_;
	bool temporarily_irreversible_;

	/////////////////////////
	// Checkpointing
	/////////////////////////
	bool checkpoint_restored_;

	bool use_classic_devs_;

	DevsTime block_outgoing_;

	std::shared_ptr<Tracers> tracers_;

	std::shared_ptr<RootDevs> model_;
	std::list<std::shared_ptr<AtomicDevs> > models_;

	Color color_;

	//////////////////////////
	// COUNTERS
	/////////////////////////
	int message_sent_count_;
	int message_received_count_;

	//////////////////////////
	// MESSAGES
	//////////////////////////
	std::deque<Message> input_message_queue_;
	std::deque<Message> output_message_queue_;

	//////////////////////////
	// GVT
	//////////////////////////
	/**
	 * Mattern's GVT algorithm
	 */
	std::map<Color, std::map<unsigned, int> > colorVectors_;

	std::array<ThreadEvent, 4> vchange_cv_;

	double tmin_;

private:
	void Send(std::shared_ptr<AtomicDevs> adevs, DevsTime time,
	        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	                std::owner_less<std::weak_ptr<Port> > > content);

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(controller_, id_, msg_copy_, has_initial_allocator_, do_some_tracing_,
		        is_simulation_irreversible_, temporarily_irreversible_, use_classic_devs_,
		        block_outgoing_, tracers_, model_, models_, message_sent_count_, message_received_count_);
	}
};

bool wayToSort(std::shared_ptr<BaseDevs> one, std::shared_ptr<BaseDevs> two);

} /* namespace ns_DEVS */

#endif /* SOLVER_H_ */
