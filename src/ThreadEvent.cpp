#include "ThreadEvent.h"

namespace cdevs {

/**
 * \brief Constructor. Sets the flag to false initially
 */
ThreadEvent::ThreadEvent()
{
	flag_ = false;
}

/**
 * \brief Destructor
 */
ThreadEvent::~ThreadEvent()
{

}

/**
 * \brief Sets the internal flag to true
 */
void ThreadEvent::Set()
{
	flag_ = true;
	cv_.notify_all();
}

/**
 * \brief Clears the internal flag
 */
void ThreadEvent::Clear()
{
	flag_ = false;
}

/**
 * \brief Waits until it is set
 *
 * @param time_out  Time in milliseconds to wait until time out occurs (default = 0)
 */
void ThreadEvent::Wait(int time_out)
{
	if (flag_)
		return;

	std::unique_lock<std::mutex> wait_lock(mutex_);

	if (time_out == 0)
		cv_.wait(wait_lock/*, [&] {return flag_;}*/);
	else
		cv_.wait_for(wait_lock, std::chrono::seconds(time_out), [&] {return flag_;});

}

/**
 * \brief Checks if the flag is set
 */
bool ThreadEvent::isSet() const
{
	return flag_;
}

} /* namespace cdevs */
