#include "GvtControlMessage.h"

/**
 * \brief Default constructor for an empty GVT control message
 */
cdevs::GvtControlMessage::GvtControlMessage() : clock_(0.0), send_(0.0)
{
}

/**
 * \brief Constructs a GVT control message
 *
 * @param clock  The clock value
 * @param send  The minimum clock value when red messages are sent
 * @param wait_vector  Wait vector that keeps track of which kernel to wait for
 * until all messages of previous color are received
 * @param accumulator  Keeps track of messages sent during current color
 */
cdevs::GvtControlMessage::GvtControlMessage(double clock, double send, std::map<unsigned, int> wait_vector,
        std::map<unsigned, int> accumulate_vector) : clock_(clock), send_(send), wait_vector_(wait_vector), accumulate_vector_(accumulate_vector)
{
}

/**
 * \brief Gets the clock value
 *
 * @return  The clock value
 */
double cdevs::GvtControlMessage::get_clock() const
{
	return clock_;
}

/**
 * \brief Gets the minimum clock value when red messages are sent
 *
 * @return  The minimum clock value when red messages are sent
 */
double cdevs::GvtControlMessage::get_send() const
{
	return send_;
}

/**
 * \brief Gets the wait vector that keeps track of which kernel to wait for
 * until all messages of previous color are received
 *
 * @return  The wait vector that keeps track of which kernel to wait for
 * until all messages of previous color are received
 */
const std::map<unsigned, int>& cdevs::GvtControlMessage::get_wait_vector() const
{
	return wait_vector_;
}

/**
 * \brief Gets the vector that keeps track of messages sent during current color
 *
 * @return The vector that keeps track of messages sent during current color
 */
const std::map<unsigned, int>& cdevs::GvtControlMessage::get_accumulate_vector() const
{
	return accumulate_vector_;
}

/**
 * \brief Sets the clock value
 *
 * @param clock  The clock value to set
 */
void cdevs::GvtControlMessage::set_clock(double clock)
{
	clock_ = clock;
}

/**
 * \brief Sets the minimum clock value when red messages are sent
 *
 * @param send  The minimum clock value when red messages are sent
 */
void cdevs::GvtControlMessage::set_send(double send)
{
	send_ = send;
}

/**
 * \brief Sets the wait vector that keeps track of which kernel to wait for
 * until all messages of previous color are received
 *
 * @param  The wait vector that keeps track of which kernel to wait for
 * until all messages of previous color are received
 */
void cdevs::GvtControlMessage::set_wait_vector(std::map<unsigned, int> wait_vector)
{
	wait_vector_ = wait_vector;
}

/**
 * \brief Gets the vector that keeps track of messages sent during current color
 *
 * @return The vector that keeps track of messages sent during current color
 */
void cdevs::GvtControlMessage::set_accumulate_vector(std::map<unsigned, int> accumlate_vector)
{
	accumulate_vector_ = accumlate_vector;
}
