/*
 * AutoAllocator_test.cpp
 *
 *  Created on: Jun 12, 2015
 *      Author: paul
 */

#include "../BaseDEVS.h"
#include "../AtomicDevs.h"
#include "gtest/gtest.h"
#include "AutoAllocator.h"
#include "atomicmodel_test/model.h"
#include "trafficlight_classic/model.h"
#include <memory>

namespace cdevs {

TEST(Alloctest, Allocation){
	AutoAllocator m;
	int n_kernels = 2;
	std::shared_ptr<AtomicDevs> model1 = cdevs_examples::atomic::TrafficLight::create("trafficSystem1");
	std::shared_ptr<AtomicDevs> model2 = cdevs_examples::atomic::TrafficLight::create("trafficSystem2");
	std::shared_ptr<AtomicDevs> model3 = cdevs_examples::atomic::TrafficLight::create("trafficSystem3");

	std::list<std::shared_ptr<AtomicDevs> > models;
	models.push_back(model1);
	models.push_back(model2);
	models.push_back(model3);
	m.Allocate(models, n_kernels);

	EXPECT_EQ(0, model1->get_location());
	EXPECT_EQ(1, model2->get_location());
	EXPECT_EQ(0, model3->get_location());


}

//TEST(ModelTest, Atomic)
//{
//	// std::string modelpath = "examples/libAtomicModelTest";
//	// std::shared_ptr<BaseDevs> model = BaseDevs::load(modelpath);
//	std::shared_ptr<BaseDevs> model = cdevs_examples::atomic::TrafficLight::create("VerkeersLicht");
//
//	ASSERT_TRUE(model != NULL);
//	EXPECT_EQ("VerkeersLicht", model->get_model_name());
//}
//
//TEST(ModelTest, TrafficLight_Classic)
//{
//	// std::string modelpath = "examples/libTrafficLight_Classic";
//	// std::shared_ptr<BaseDevs> model = BaseDevs::load(modelpath);
//
//	std::shared_ptr<BaseDevs> model = cdevs_examples::atomic::TrafficLight::create("trafficSystem");
//
//	ASSERT_TRUE(model != NULL);
//	EXPECT_EQ("trafficSystem", model->get_model_name());
//}

} /* namespace ns_DEVS */


