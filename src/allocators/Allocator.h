#ifndef ALLOCATOR_H_
#define ALLOCATOR_H_

#include <vector>
#include <list>
#include "../utility.h"
#include "../AtomicDevs.h"

namespace cdevs {
class Allocator
{
public:
	virtual ~Allocator() = default;
	virtual void Allocate(std::list<std::shared_ptr<AtomicDevs>> models, int nr_nodes) = 0;
	virtual int GetTerminationTime() =0;
};
}
#endif
