#include "Allocator.h"
#include <vector>

namespace cdevs {
/**
 * Allocate all models in a static manner, simply trying to divide the number of models equally.
 * Our 'heuristic' is to allocate in chunks as defined in the root coupled model.
 */

class AutoAllocator: public Allocator
{
public:
	AutoAllocator();

	void Allocate(std::list<std::shared_ptr<AtomicDevs>> models, int nr_nodes);
	int GetTerminationTime();

	virtual ~AutoAllocator();
};
}
