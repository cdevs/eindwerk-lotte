#ifndef TRACERVERBOSE_H_
#define TRACERVERBOSE_H_

#include <fstream>
#include <string>
#include <vector>
#include <mutex>
#include <condition_variable>

#include "../tracers/Tracer.h"
#include "../utility.h"

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/polymorphic.hpp>

namespace cdevs {

class TracerVerbose: public Tracer
{
	friend class TracerVerboseTest;

	/*
	 * A Tracer that traces the simulation in a verbose way.
	 */
public:
	TracerVerbose(std::shared_ptr<Controller> controller, std::string filename="");
	TracerVerbose();
	virtual ~TracerVerbose();

	void TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t);
	void TraceInternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceExternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS);
	void Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text);

private:
	DevsTime prevtime_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<Tracer>(this), prevtime_);
	}
};


} /* namespace cdevs */

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs::TracerVerbose)

#endif /* TRACERVERBOSE_H_ */
