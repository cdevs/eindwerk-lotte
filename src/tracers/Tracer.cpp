#include "Tracer.h"
#include "../exceptions/LogicDevsException.h"

namespace cdevs {
/*
 * \brief Tracer constructor, used for Custom Tracers because it needs no Controller
 */

Tracer::Tracer(std::string filename)
	: controller_(),  filename_(filename), stream_(nullptr)
{
}

/*
 * \brief Tracer constructor, sets filename
 * @param filename: Name of the file the tracer should write to. If
 * left empty, it writes to the standard output
 */
Tracer::Tracer(std::shared_ptr<Controller> controller, std::string filename)
	: controller_(controller), filename_(filename), stream_(nullptr)
{
}

/*
 * \brief Creates the stream to write to, either by opening a file,
 * or by referencing std::cout
 * @param recover: false if starting a new simulation, true if
 * continuing from an interrupted one.
 */
void Tracer::StartTracer(bool recover)
{
	if (filename_ == "") {
		stream_ = &std::cout;
	} else if (recover) {
		stream_ = new std::ofstream(filename_, std::ofstream::out | std::ios::app);
	} else {
		stream_ = new std::ofstream(filename_, std::ofstream::out | std::ios::out);
	}
}

/*
 * \brief Stops the tracer. If not using std::cout, deletes the stream.
 */
void Tracer::StopTracer()
{
	if (stream_ != &std::cout) {
		delete stream_;
	}
	stream_ = nullptr;
}

/*
 * \brief Tracer destructor, if a stream was created, destroys it.
 */
Tracer::~Tracer()
{
	if (stream_ && (stream_ != &std::cout)) {
		delete stream_;
	}
	stream_ = nullptr;

}

/*
 * \brief Prints the string to the write medium
 * @param text: The string to print
 */
void Tracer::PrintString(std::string text) const
{
	if (stream_ == nullptr)
		throw LogicDevsException("Tracer::PrintString: Tracer was not started!\n" + text);
	*stream_ << text;
}

void Tracer::Flush()
{
	stream_->flush();
}

void Tracer::set_controller(std::shared_ptr<Controller> controller)
{
	controller_ = controller;
}

} /* namespace cdevs */
