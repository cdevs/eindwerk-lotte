#ifndef TRACERXML_H_
#define TRACERXML_H_

#include <vector>
#include "../BaseDEVS.h"
#include "../tracers/Tracer.h"

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/polymorphic.hpp>

namespace cdevs {

class TracerXml: public Tracer
{
	friend class TracerXmlTest;
	/*
	 * The Tracer generates an XML-file describing the simulation
	 */
public:
	using Tracer::Tracer;
	virtual ~TracerXml();

	void StartTracer(bool recover);
	void StopTracer();
	void TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t);
	void TraceInternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceExternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS);
	void Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string name, std::string text);

	void Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text);
private:
	void RunEventTraceAtController(std::shared_ptr<AtomicDevs> model, std::string text);

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<Tracer>(this));
	}
};

}

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs::TracerXml)

#endif /* TRACERXML_H_ */
