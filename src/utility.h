/*
 * utility.h
 *
 *  Created on: Mar 12, 2015
 *      Author: uauser
 */

#ifndef UTILITY_H_
#define UTILITY_H_

#include <functional>
#include <memory>
#include <string>

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/archives/binary.hpp>
#include "DevsTime.h"

namespace cereal {
class access;
} /* namespace cereal */

namespace cdevs {

/**
 * The abstract class for model states.
 */
class StateBase
{
public:
	DevsTime time_last_;
	DevsTime time_next_;
	DevsTime elapsed_;

	/*bool friend operator< (const State& lhs, const State& rhs)
	 {
	 return (lhs.value < rhs.value );
	 }*/

	virtual ~StateBase();

	virtual StateBase & clone() const = 0;

	virtual std::string string() const;
	virtual std::string toXML() const;
	virtual std::string toJson() const;
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(time_last_, time_next_, elapsed_);
	}
};

/**
 * \brief State uses the 'Curiously Recurring Template Pattern' to implement a
 * polymorphic copy method for StateBase.
 */
template<class Derived = StateBase>
class State: public StateBase
{
public:
	/**
	 * \brief Return a reference to a copy of this object.
	 *
	 * @return A reference to a new copy of this object.
	 */
	State<Derived> & clone() const
	{
		return *(new Derived(const_cast<Derived &>(static_cast<Derived const&>(*this))));
	}

	/////////////////////////////////////////////////////////
	// CRTP style model factory methods
	/////////////////////////////////////////////////////////
	template<typename ... T>
	static std::shared_ptr<Derived> create(T&& ... all)
	{
		return std::shared_ptr < Derived > (new Derived(std::forward<T>(all)...));
	}
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<StateBase>(this));
	}
};

/**
 * The interface for events, outputted from one model to another on external transitions.
 */
class EventBase
{
public:
	virtual ~EventBase() = default;
	virtual EventBase const& cast() const = 0;
	virtual int getType() const = 0;
	virtual std::string string() const;
	virtual std::string toXML() const;
	virtual std::string toJson() const;
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
	}
};

/**
 * The templated child of EventBase. This provides a method to cast this directly
 * to the derived class.
 */
template<typename Derived, int UID>
class EventCRTP: public EventBase
{
public:
	EventBase const& cast() const;
	int getType() const;
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<EventBase>(this));
	}
};

class Event
{
public:
	unsigned event_size_;
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(event_size_);
	}
};

/**
 * \brief Destructor
 */
inline StateBase::~StateBase()
{
}

/**
 * \brief Base implementation for converting a state to a string
 *
 * @return  Default: empty string
 */
inline std::string StateBase::string() const
{
	return "";
}

/**
 * \brief Base implementation for converting a state to an XML
 *
 * @return  Default: empty XML
 */
inline std::string StateBase::toXML() const
{
	return "";
}

/**
 * \brief Base implementation for converting a state to a JSON
 *
 * @return  Default: empty JSON
 */
inline std::string StateBase::toJson() const
{
	return "";
}

/**
 * \brief Base implementation for converting an event to a string
 *
 * @return  Default: empty string
 */
inline std::string EventBase::string() const
{
	return "";
}

/**
 * \brief Base implementation for converting an event to an XML
 *
 * @return  Default: empty XML
 */
inline std::string EventBase::toXML() const
{
	return "";
}

/**
 * \brief Base implementation for converting an event to a JSON
 *
 * @return  Default: empty JSON
 */
inline std::string EventBase::toJson() const
{
	return "";
}

/**
 * \brief Casts the event base to an EventCRTP
 *
 * See CRT pattern
 *
 * @return  A converted EventBase object
 */
template<typename Derived, int UID>
EventBase const& EventCRTP<Derived, UID>::cast() const
{
	return static_cast<EventCRTP<Derived, UID> const&>(*this);
}

/**
 * \brief Gets the type of the EventCRTP
 *
 * See CRT pattern
 *
 * @return  The type of the EventCRTP
 */
template<typename Derived, int UID>
int EventCRTP<Derived, UID>::getType() const
{
	return UID;
}

}

CEREAL_REGISTER_TYPE(cdevs::EventBase)

#endif /* UTILITY_H_ */
