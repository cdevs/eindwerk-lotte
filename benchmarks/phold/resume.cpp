/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "PHOLD.h"
#include "Simulator.h"

using namespace cdevs;

int main(int argc, char * argv[])
{
	auto sim = Simulator::LoadFromCheckpoint("phold", 100);

	return 0;
}
