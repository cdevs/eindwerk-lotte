#ifndef BENCHMARKS_PHOLD_H_
#define BENCHMARKS_PHOLD_H_

#include "../../src/AtomicDevs.h"
#include "../../src/CoupledDEVS.h"
#include <utility>
#include <string>
#include <list>
#include <random>
#include <limits>
#include <algorithm>

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/common.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/tuple.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/details/polymorphic_impl.hpp>

namespace cdevs_examples {
namespace phold {

typedef std::list<std::shared_ptr<const cdevs::EventBase> > Outbag;
typedef std::map<std::weak_ptr<cdevs::Port>, Outbag, std::owner_less<std::weak_ptr<cdevs::Port> > > Outbags;

class PholdEvent: public cdevs::EventCRTP<PholdEvent, 0>
{
	friend class cereal::access;
	friend class HeavyPHOLDProcessor;
public:
	PholdEvent(int size = 2);
	std::string string() const
	{
		return std::to_string(payload_.size());
	}
protected:
	std::vector<int> payload_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::EventCRTP<PholdEvent, 0> >(this), payload_);
	}
};

class PHOLDModelState: public cdevs::State<PHOLDModelState>
{
	friend class HeavyPHOLDProcessor;
	friend class cereal::access;
public:
	PHOLDModelState();
	std::string string() const
	{
		std::string t = "";
		if (events_.empty())
			return "";
		for (auto p : events_) {
			t += "\n\t\t" + std::get<0>(p)->string() + "\t" + std::to_string(std::get<1>(p));
		}
		return t;
	}
protected:
	std::list<std::tuple<std::shared_ptr<PholdEvent>, double> > events_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<PHOLDModelState> >(this), events_);
	}
};

double getProcTime(std::shared_ptr<PholdEvent> event);
int getNextDestination(std::shared_ptr<PholdEvent> event, int nodenum, std::vector<int> local, std::vector<int> remote,
        double percentageremote);
int getRand(std::shared_ptr<PholdEvent> event);

class HeavyPHOLDProcessor: public cdevs::Atomic<HeavyPHOLDProcessor, PHOLDModelState>
{
	friend class PHOLD;
	friend class cereal::access;
public:
	HeavyPHOLDProcessor(std::string name, unsigned iterations, unsigned totalAtomics, int modelnumber,
	        std::vector<int> local, std::vector<int> remote, double percentageremotes, int eventsize);
	PHOLDModelState & ExtTransition(Outbags inputs);
	PHOLDModelState & IntTransition();
	PHOLDModelState & ConfTransition(Outbags inputs);
	Outbags OutputFunction();

	double TimeAdvance();
protected:
	HeavyPHOLDProcessor()
		: HeavyPHOLDProcessor("", 0, 0, 0, std::vector<int>(), std::vector<int>(), 0, 0)
	{
	}
	std::weak_ptr<cdevs::Port> inport_;
	double percentageRemote_;
	std::vector<std::weak_ptr<cdevs::Port>> outports_;
	unsigned totalAtomics_;
	int modelnumber_;
	unsigned iterations_;
	std::vector<int> local_;
	std::vector<int> remote_;
	int eventsize_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<HeavyPHOLDProcessor, PHOLDModelState>>(this), inport_,
		        percentageRemote_, outports_, totalAtomics_, modelnumber_, iterations_, local_, remote_,
		        eventsize_);
	}
};

class PHOLD: public cdevs::Coupled<PHOLD>
{
	friend class cereal::access;
public:
	PHOLD(unsigned nodes, unsigned atomicsPerNode, unsigned iterations, double percentageremotes, int eventsize,
	        bool distributed = false);
protected:
	PHOLD()
		: PHOLD(0, 0, 0, 0, 0)
	{
	}
	std::vector<std::shared_ptr<HeavyPHOLDProcessor>> processors_;
	int eventsize_;
	bool distributed_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Coupled<PHOLD> >(this), processors_, eventsize_, distributed_);
	}
};

}
}

CEREAL_REGISTER_TYPE(cdevs_examples::phold::PholdEvent)
CEREAL_REGISTER_TYPE(cdevs_examples::phold::HeavyPHOLDProcessor)
CEREAL_REGISTER_TYPE(cdevs_examples::phold::PHOLD)

#endif /* BENCHMARKS_PHOLD_H_ */
